import 'normalize.css';
import React from 'react';
import { Switch, Route, BrowserRouter } from 'react-router-dom';
import Container from './layout/Container';
import Fonts from './layout/Fonts';
import Landing from './landing/Landing';
import Menu from './layout/menu/Menu';

function App() {
  return (
    <>
      <Fonts />
      <BrowserRouter>
        <Container>
          <Menu />
          <Switch>
            <Route
              path="/"
              exact
              component={Landing}
            />
            <Route
              path="*"
              exact
              component={() => <span>404</span>}
            />
          </Switch>
        </Container>
      </BrowserRouter>
    </>
  );
}

export default App;
