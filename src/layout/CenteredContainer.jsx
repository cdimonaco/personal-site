import styled from '@emotion/styled';

const CenteredContainer = styled.div`
  display: flex;
  position: fixed;
  flex-direction: column;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
`;

export default CenteredContainer;
