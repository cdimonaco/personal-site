import React from 'react';
import { Global, css } from '@emotion/core';

const fontsCss =  css`
  @import url('https://fonts.googleapis.com/css?family=Montserrat:400,600&display=swap');
`

const Fonts = () => <Global styles={fontsCss} />

export default Fonts;
