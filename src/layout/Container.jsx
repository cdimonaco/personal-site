import styled from '@emotion/styled';

const Container = styled.div`
  height: 100%;
  width: 100%;
  font-family: Montserrat;
  background-color: #2f3640;
  position: fixed;
  color: white;
  top: 0;
  left: 0;
  overflow: auto;
  z-index: -1;
`

export default Container;