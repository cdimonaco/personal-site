import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const Icon = ({
  icon
}) => (
  <FontAwesomeIcon 
    style={{
      height: '1.5rem',
      width: '1.5rem'
    }}
    icon={icon}
  />
);

export default Icon;
