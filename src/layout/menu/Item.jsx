import React from 'react';
import styled from '@emotion/styled';
import { NavLink } from 'react-router-dom';

const ItemStyled = styled.li`
  color: ${props => props.active ? '#fbc531' : 'white'};
`
const Item = ({
  linkTo,
  children,
  active
}) => (
  <NavLink
    to={linkTo}
  >
    <ItemStyled active={active}>
      {children}
    </ItemStyled>
  </NavLink>
);

export default Item;