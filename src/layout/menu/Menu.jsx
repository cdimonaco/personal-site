import React from 'react';
import styled from '@emotion/styled';
import ItemsContainer from './ItemsContainer';
import Item from './Item';

const MenuContainer = styled.div`
  display: flex;
  justify-content: flex-end;
`;


const Menu = () => (
  <MenuContainer>
    <ItemsContainer>
      <Item linkTo="#"> about me </Item>
      <Item linkTo="/mywork"active> my work </Item>
      <Item linkTo="#"> contact me</Item>
    </ItemsContainer>
  </MenuContainer>
);

export default Menu;