import styled from '@emotion/styled';

const ItemsContainer = styled.ul`
  list-style: none;
  display: flex;
  padding: 0;
  width: 400px;
  justify-content: space-around;
  @media screen and (max-width : 768px)  {
    width: 500px;
  }
`;

export default ItemsContainer;
