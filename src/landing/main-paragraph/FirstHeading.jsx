import React from 'react';
import styled from '@emotion/styled';

const FirstHeadingStyled = styled.h1`
  max-width: 215px;
  @media screen and (max-width : 768px)  {
    font-size: 1.5rem;
  }
`;

const Name = styled.span`
  color:  #fbc531;
`;

const FirstHeading = () => (
  <FirstHeadingStyled>
    Hi, I'm <Name> Carmine </Name>
  </FirstHeadingStyled>
);

export default FirstHeading;
