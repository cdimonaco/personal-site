import React from 'react';
import styled from '@emotion/styled';

const SecondHeadingStyled = styled.h2`
  max-width: 273px;
  @media screen and (max-width : 768px)  {
    font-size: 1.1rem;
    white-space: nowrap;
  }
`;

const SecondHeading = () => (
  <SecondHeadingStyled>
    <div>a full stack web</div> 
    <div>developer.</div>
  </SecondHeadingStyled>
);

export default SecondHeading;
