import React from 'react';
import styled from '@emotion/styled';
import SecondHeading from './SecondHeading';
import FirstHeading from './FirstHeading';


const MainParagraphContainer = styled.div`
  align-content: center;
  display: flex;
  @media screen and (max-width : 768px)  {
    min-width: 300px;
  } 
`;

const MainParagraph = () => (
  <MainParagraphContainer>
    <FirstHeading />
    <SecondHeading />
  </MainParagraphContainer>
);

export default MainParagraph;
