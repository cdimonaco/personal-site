import React from 'react';
import SocialIcon from '../../layout/Icon'
import { faGithub } from '@fortawesome/free-brands-svg-icons';

const Github = ({
  icon
}) => <SocialIcon icon={faGithub} />

export default Github;
