import React from 'react';
import styled from '@emotion/styled';
import Github from './Github';
import Linkedin from './Linkedin';
import Instagram from './Instagram';
import Email from './Email';

const SocialIconsContainer = styled.div`
  display: flex;
  
  svg {
    padding-right: 1rem;
  }
`

const SocialIcons = () => (
  <SocialIconsContainer>
    <Github />
    <Linkedin />
    <Instagram />
    <Email />
  </SocialIconsContainer>
)
export default SocialIcons;
