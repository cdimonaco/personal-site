import React from 'react';
import SocialIcon from '../../layout/Icon'
import { faEnvelope } from '@fortawesome/free-solid-svg-icons';

const Email = ({
  icon
}) => <SocialIcon icon={faEnvelope} />

export default Email;
