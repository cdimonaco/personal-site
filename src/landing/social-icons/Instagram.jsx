import React from 'react';
import SocialIcon from '../../layout/Icon'
import { faInstagram } from '@fortawesome/free-brands-svg-icons';

const Instagram = ({
  icon
}) => <SocialIcon icon={faInstagram} />

export default Instagram;
