import React from 'react';
import SocialIcon from '../../layout/Icon'
import { faLinkedin } from '@fortawesome/free-brands-svg-icons';

const Linkedin = ({
  icon
}) => <SocialIcon icon={faLinkedin} />

export default Linkedin;
