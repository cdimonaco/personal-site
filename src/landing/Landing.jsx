import React from 'react';
import MainParagraph from './main-paragraph/MainParagraph';
import SocialIcons from './social-icons/SocialIcons';
import CenteredContainer from '../layout/CenteredContainer';

const Landing = () => (
  <CenteredContainer>
    <MainParagraph /> 
    <SocialIcons />
  </CenteredContainer>
);

export default Landing;
