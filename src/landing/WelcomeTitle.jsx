import React from 'react';
import styled from '@emotion/styled';
import CenteredContainer from './CenteredContainer';
import SocialIcons from './social-icons/SocialIcons';

const WelcomeTitleContainer = styled.div`
  align-items: center;
  display: flex;
  .first {
    max-width: 215px;
  }
  .second {
    max-width: 273px;
  }
  .name {
    color: #fbc531;
  }
  @media screen and (max-width : 768px)  {
    min-width: 300px;
    .second {
      font-size: 1.1rem;
      white-space: nowrap;
    }
    .first {
      font-size: 1.5rem;
    }
  }
`

const WelcomeTitle = () => (
  <CenteredContainer>
    <WelcomeTitleContainer>
      <h1 className="first">Hi, I'm <span className="name">Carmine</span></h1>
      <h2 className="second">
        <div>a full stack web</div> <div>developer.</div>
      </h2>
    </WelcomeTitleContainer>
    <SocialIcons />
  </CenteredContainer>
)

export default WelcomeTitle;
